NAME = gwkoji
VERSION = 0.1.0

sources:
	git archive --format=tar --prefix=${NAME}-${VERSION}/ tags/v${VERSION} | tar xf -
	python3.6 ${NAME}-${VERSION}/setup.py sdist
	mv dist/${NAME}-${VERSION}.tar.gz .
	rm -rf ${NAME}-${VERSION} dist
	rpmbuild -D '_srcrpmdir ./' -D '_sourcedir ./' -bs gwkoji.spec