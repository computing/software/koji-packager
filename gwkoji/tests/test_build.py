# -*- coding: utf-8 -*-
# Copyright(C) 2019 Adam Mercer <adam.mercer@ligo.org>
#
# This file is part of gwkoji.
#
# gwkoji is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Test suite for gwkoji.build
"""

from unittest import mock

from .. import build as gwkoji_build


@mock.patch('gwkoji.build.find_executable', return_value='/usr/bin/spectool')
@mock.patch('gwkoji.build.logged_check_call')
def test_download_spec_sources(call, _):
    gwkoji_build.download_spec_sources('test.spec', 'outdir', logger=1)
    call.assert_called_once_with(
        ['/usr/bin/spectool', '--get-files', '--directory',
         'outdir', 'test.spec'],
        logger=1,
    )


@mock.patch('gwkoji.build.find_executable', return_value='/usr/bin/rpmbuild')
def test_rpmbuild_args(_):
    args = gwkoji_build._rpmbuild_args(
        '-bs', 'foobar-1.0-1.spec', tmpdir='/tmp'
    )
    assert args == [
        '/usr/bin/rpmbuild', '--verbose', '--define', '_topdir /tmp',
        '-bs', 'foobar-1.0-1.spec'
    ]


@mock.patch('gwkoji.build.find_executable', return_value='/usr/bin/rpmbuild')
def test_rpmbuild_args_none(_):
    args = gwkoji_build._rpmbuild_args('-bs', 'foobar-1.0-1.spec', tmpdir=None)
    assert args == [
        '/usr/bin/rpmbuild', '--verbose', '-bs', 'foobar-1.0-1.spec'
    ]
