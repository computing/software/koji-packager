FROM ligo/base:el7

LABEL name="Koji Packager" \
      maintainer="Adam Mercer <adam.mercer@ligo.org>" \
      date="20190518" \
      support="Best Effort"

# copy RPM to container
COPY dist /dist

# switch to upstream EPEL
RUN yum -y install epel-release && \
      rm /etc/yum.repos.d/lscsoft-epel.repo && \
      rm -rf /var/cache/yum/x86_64/7/lscsoft-epel

# install updates
RUN yum makecache && yum -y update

# install koji-packager
RUN yum -y localinstall /dist/el7/python36-gwkoji-*.rpm && \
      rm -rf /dist

# install extra deps
RUN yum -y install \
            git2u \
            git-lfs \
            sudo && \
      yum clean all

# setup environment
COPY /environment/sudoers.d/albert /etc/sudoers.d/albert
RUN chmod 0640 /etc/sudoers.d/albert
COPY /environment/koji.conf /etc/koji.conf
COPY /environment/krb5.conf /etc/krb5.conf
COPY /environment/mock /etc/mock
RUN mkdir /container

# setup user
RUN useradd -m -d /container/albert -s /bin/bash albert
RUN su - albert -c "git lfs install"

# enter shell
USER albert
WORKDIR /container/albert
CMD ["/bin/bash", "-l"]