# -*- coding: utf-8 -*-
# Copyright(C) 2019 Adam Mercer <adam.mercer@ligo.org>
#
# This file is part of gwkoji.
#
# gwkoji is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import pathlib
import sys
from functools import wraps
from subprocess import (check_call, check_output, call, DEVNULL)


def _logged_call(func):
    @wraps(func)
    def decorated(cmd, *args, logger=logging.getLogger(),
                  loglevel=logging.DEBUG, **kwargs):
        # get logging level
        if not isinstance(loglevel, int):
            loglevel = logging.getLevelName(loglevel)

        # set default streams based on logging level
        if logger.getEffectiveLevel() > logging.INFO:
            kwargs.setdefault('stderr', DEVNULL)
            if func != check_output:
                kwargs.setdefault('stdout', DEVNULL)
        elif func != check_output:
            kwargs.setdefault('stdout', sys.stderr)

        # log the command
        logger.log(loglevel, "$ {0}".format(" ".join(cmd)))

        # run it
        return func(cmd, *args, **kwargs)
    return decorated


logged_call = _logged_call(call)
logged_check_call = _logged_call(check_call)
logged_check_output = _logged_call(check_output)


def source_type(source):
    """Determine the source type based on its file name

    Parameters
    ----------
    source : `pathlib.Path`
        the source `Path`

    Returns
    -------
    type_ : `str`
        the source type, one of

        - ``'git'``
        - ``'spec'``
        - ``'srcrpm'``
        - ``'tarball'``

    Raises
    ------
    ValueError
        if the source type is not recognised
    """
    source = pathlib.Path(source)
    name = source.name
    if name.endswith('.src.rpm'):
        return 'srcrpm'
    if name.endswith('.spec'):
        return 'spec'
    if name.endswith('.tar') or source.stem.endswith('.tar'):
        return 'tarball'
    if str(source).startswith(('git+https', 'git+ssh')):
        return 'git'
    raise ValueError(
        "failed to determine source type for '{0}'".format(source),
    )
