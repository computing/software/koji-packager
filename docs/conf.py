# -*- coding: utf-8 -*-
#
# gwkoji documentation build configuration file

import re
from pathlib import Path

from gwkoji import __version__ as VERSION

extensions = [
    'sphinx.ext.intersphinx',
    'sphinx.ext.napoleon',
    'sphinxarg.ext',
]

# templates_path = ['_templates']

source_suffix = '.rst'

master_doc = 'index'

# General information about the project.
project = u'gwkoji'
copyright = u'2019, Adam Mercer'
author = u'Adam Mercer'

# The short X.Y version.
version = re.split(r'[\w-]', VERSION)[0]
# The full version, including alpha/beta/rc tags.
release = VERSION

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This patterns also effect to html_static_path and html_extra_path
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'monokai'

# Intersphinx directory
intersphinx_mapping = {
    'python': ('https://docs.python.org/', None),
    'koji': ('https://docs.pagure.org/koji/', None),
}

# The reST default role (used for this markup: `text`) to use for all
# documents.
default_role = 'obj'

# napoleon configuration
napoleon_use_rtype = False

# Don't inherit in automodapi
numpydoc_show_class_members = False
automodapi_inherited_members = False

# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']

# Output file base name for HTML help builder.
htmlhelp_basename = 'gwkojidoc'


# -- apidoc ---------------------------------------------------------------

def run_apidoc(_):
    """Call sphinx-apidoc
    """
    from sphinx.ext.apidoc import main as apidoc_main
    curdir = Path(__file__).parent.resolve()
    module = curdir.parent / "gwkoji"
    apidoc_main([
        '--separate',
        '--force',
        '--output-dir',
        str(curdir),
        str(module),
        str(module / "tests"),
    ])


# -- setup --------------------------------------------------------------------

def setup(app):
    app.connect('builder-inited', run_apidoc)
