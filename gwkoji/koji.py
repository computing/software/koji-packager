# -*- coding: utf-8 -*-
# Copyright(C) 2019 Adam Mercer <adam.mercer@ligo.org>
#
# This file is part of gwkoji.
#
# gwkoji is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Interactions with koji
"""

from distutils.spawn import find_executable

from .utils import logged_check_call


def _koji_call(
        *args,
        **kwargs,
):
    koji = find_executable('koji')
    if koji is None:
        raise FileNotFoundError("No such file or directory: 'koji'")
    cmd = (koji,) + args
    return logged_check_call(cmd, **kwargs)


def moshimoshi(*args, **kwargs):
    return _koji_call('moshimoshi', *args, **kwargs)


def list_pkgs(*args, **kwargs):
    return _koji_call('list-pkgs', *args, **kwargs)


def add_pkg(*args, **kwargs):
    # ensure --owner option is specified
    args = ('--owner',) + args
    return _koji_call('add-pkg', *args, **kwargs)


def build(*args, **kwargs):
    return _koji_call('build', *args, **kwargs)
