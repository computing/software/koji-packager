########################################
 gwkoji-packager command-line interface
########################################

.. argparse::
   :module: gwkoji.options
   :func: parser
   :prog: gwkoji-packager
